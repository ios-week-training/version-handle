import SwiftUI

struct CameraView: UIViewControllerRepresentable {
    @Binding var showGallery: Bool
    var sourceType:Int
    
    init(showGallery: Binding<Bool>, sourceType: Int) {
        self._showGallery = showGallery
        self.sourceType = sourceType
    }
    
    func makeUIViewController(context: Context) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        picker.sourceType = sourceType == 0 ? .camera : .photoLibrary
        
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {
        
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        var parent: CameraView
        
        init(_ parent: CameraView) {
            self.parent = parent
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            parent.showGallery = false
        }
        
    }
}
