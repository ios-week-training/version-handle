//
//  ContentView.swift
//  app1
//
//  Created by Iqbal Rahman on 05/03/24.
//

import SwiftUI

struct ContentView: View {
    @StateObject private var dataList: ModifyData = ModifyData()
    @AppStorage("login") var isLogin: Bool = false
    
    var body: some View {
        NavigationView {
            VStack {
                Toggle("", isOn: $isLogin)
                ScrollView {
                    LazyVStack {
                        ForEach(dataList.data) { data in
                            NavigationLink {
                                    PersonDetail(person: data)
                                } label : {
                                    ListAdaptor(person: data)
                                }
                            }
                            .navigationTitle("Person List")
                        }
                    }
            }
        }
        .environmentObject(dataList)
    }
}

#Preview {
    let previewUserDefaults: UserDefaults = {
        let d = UserDefaults(suiteName: "preview_user_defaults")!
        d.set("Petr", forKey: "profileName")
        return d
    }()

    return ContentView()
        .defaultAppStorage(previewUserDefaults)
}
