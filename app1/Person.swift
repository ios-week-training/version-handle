//
//  Person.swift
//  app1
//
//  Created by Iqbal Rahman on 05/03/24.
//

import Foundation

struct Person: Identifiable {
    let id = UUID()
    let firstName: String
    let lastName: String
    let email: String
    let age: Int
    var fullName: String {
        "\(firstName) \(lastName)"
    }
    
    init(firstName: String, lastName: String = "", email: String, age: Int) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.age = age
    }
    
    func getFullName() -> String {
        return "\(firstName) \(lastName)"
    }
}
