//
//  PersonDetail.swift
//  app1
//
//  Created by Iqbal Rahman on 05/03/24.
//
import PhotosUI
import SwiftUI

struct PersonDetail: View {
    var person: Person
    @State var isAction: Bool = false
    @State var shoGallery: Bool = false
    
    init(person: Person) {
        self.person = person
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            Image("ProfileImage")
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                .overlay(alignment: .bottomTrailing) {
                    if #available(iOS 16.0, *) {
                        PhotoPicker(isAction: $isAction)
                    } else {
                        Image(systemName: "pencil.circle.fill")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 40)
                            .onTapGesture {
                                isAction = true
                            }
                    }
                }
            HStack {
                Text("\(person.firstName) \(person.lastName)")
                    .bold()
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                Text("\(person.age)")
            }
            Text("\(person.email)")
            Spacer()
        }
        .background(
            Group {
                if #unavailable(iOS 15.0) {
                    NavigationLink("", destination: CameraView(showGallery: $shoGallery, sourceType: 1).navigationBarBackButtonHidden(), isActive: $shoGallery)
                    .actionSheet(isPresented: $isAction) {
                        ActionSheet(
                            title: Text("Select Image"),
                            message: Text("Please select an image from the gallery or use the camera."),
                            buttons: [
                                .default(Text("Camera")) {
                                },
                                .default(Text("Galery")) {
                                    shoGallery = true
                                },
                                .cancel()
                            ]
                        )
                    }
                    
                } else {
                    EmptyView()
                    
                }
            }
        )
    }
}

#Preview {
    NavigationView {
        PersonDetail(
            person: Person(firstName: "Iqbal", lastName: "rahman", email: "iqbal@nexsoft.co.id", age: 21)
        )
    }
}
