//
//  PhotoPicker.swift
//  app1
//
//  Created by Iqbal Rahman on 05/03/24.
//

import SwiftUI
import _PhotosUI_SwiftUI

@available(iOS 16.0, *)
struct PhotoPicker: View {
    @State private var avatarItem: PhotosPickerItem?
    @Binding private var isAction: Bool
    
    init(isAction: Binding<Bool>) {
        self._isAction = isAction
    }
    
    var body: some View {
        PhotosPicker(selection: $avatarItem) {
            Image(systemName: "pencil.circle.fill")
                .resizable()
                .scaledToFit()
                .frame(width: 40)
        }
        
    }
}

#Preview {
    if #available(iOS 16.0, *) {
        PhotoPicker(isAction: .constant(false))
    } else {
        Text("ios 15")
    }
}
