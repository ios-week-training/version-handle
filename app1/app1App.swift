//
//  app1App.swift
//  app1
//
//  Created by Iqbal Rahman on 05/03/24.
//

import SwiftUI

@main
struct app1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
