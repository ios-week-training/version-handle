//
//  listAdaptor.swift
//  app1
//
//  Created by Iqbal Rahman on 05/03/24.
//

import SwiftUI

struct ListAdaptor: View {
    private let person: Person
    
    init(person: Person) {
        self.person = person
    }
    
    var body: some View {
        HStack() {
//            image
            Image("ProfileImage")
                .resizable()
                .frame(width: 100, height: 100)
                .clipShape(Circle())
            
//            text
            VStack(alignment: .leading, spacing: 10) {
                HStack(spacing: 35) {
                    Text(person.fullName)
                    Text("\(person.age)")
                }
                Text(person.email)
            }
            Spacer()
        }
        .padding()
        .background(Color.gray.opacity(0.3))
        .clipShape(RoundedRectangle(cornerRadius: 15))
        .padding(.horizontal)
        
    }
}

#Preview {
    ListAdaptor(
        person: Person(firstName: "Iqbal", lastName: "rahman", email: "iqbal@nexsoft.co.id", age: 21)
    )
}
