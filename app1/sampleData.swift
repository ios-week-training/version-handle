//
//  sampleData.swift
//  app1
//
//  Created by Iqbal Rahman on 05/03/24.
//

import Foundation

class ModifyData: ObservableObject {
    @Published var data = [Person]()
    @Published var isOn: Bool = false
    
    init() {
        addData()
    }
    
    func addData() {
        for i in 1...10 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1 ) {
                self.data.append(Person(firstName: "User", lastName: "\(i)", email: "email\(i)@nexsoft.co.id", age: 20))
            }
        }
    }
}
